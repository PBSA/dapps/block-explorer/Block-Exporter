import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link, TableRow, TableCell} from '@material-ui/core';
import styles from './styles.css';
class TransactionRow extends Component {
	findAccountName(id) {
		const accountName = this.props.accounts ? this.props.accounts.find(el => el.account_id === id) : '';
		return accountName ? <span><Link className="d-inline p-0" href={`/accountAllDetail/${accountName.account_name}`}>{accountName.account_name}</Link></span>  : <span>Account Id: {id}</span>;
	}
	
	linkAccountName(accountName) {
		return accountName ? <span><Link className="d-inline p-0" href={`/accountAllDetail/${accountName}`}>{accountName}</Link></span> : accountName;
	}
  
	renderOther(operationType, parsedTransaction, i) {
		if(parsedTransaction.fee !== undefined) {
			return (
				<TableCell> 
					<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
					<div>ID: {parsedTransaction.id}</div> 
					<div>fee: {parsedTransaction.fee.amount}</div> 			
				</TableCell>
			);
		}
	}
  
	displayOperation( operationType ) {
		const selectedOperation = this.props.operations.find(operation => operation.id === operationType);
		if(selectedOperation) {
			return selectedOperation.friendly_name;
		} else {}

		return;
	}

	renderTransaction(transaction, i) {
		const operationType = JSON.parse(transaction.operations)[0];
		const parsedTransaction = JSON.parse(transaction.operations)[1];
		parsedTransaction.id = transaction.id;
		switch(operationType) {
			case 0:
				const senderAccount = this.findAccountName(parsedTransaction.from);
				const receiverAccount = this.findAccountName(parsedTransaction.to);
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div> 
						<div>fee: {parsedTransaction.fee.amount}</div> 
						<div><strong>{receiverAccount}</strong> from <strong>{senderAccount}</strong></div>		
					</TableCell> 
				);
			case 5:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div> 
						<div>
							<strong>fee: {parsedTransaction.fee.amount}</strong> paid by <strong>{this.findAccountName(parsedTransaction.registrar)}</strong> for {this.displayOperation(operationType)} <strong>{this.linkAccountName(parsedTransaction.name)}</strong>
						</div>
					</TableCell>
				);
			case 6:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div> 
						<div>
							<strong>fee: {parsedTransaction.fee.amount}</strong> {this.displayOperation(operationType)} <strong>{this.findAccountName(parsedTransaction.account)}</strong>
						</div>
					</TableCell>
				);
			case 8:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div> 
						<div>
							<strong>fee: {parsedTransaction.fee.amount}</strong> {this.displayOperation(operationType)} <strong>{this.findAccountName(parsedTransaction.account_to_upgrade)}</strong>
						</div>
					</TableCell>
				);
			case 20:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div>
						<div>
							<strong>fee: {parsedTransaction.fee.amount}</strong> {this.displayOperation(operationType)} <strong>{this.findAccountName(parsedTransaction.witness_account)}</strong>
						</div> 
					</TableCell>
				);
			case 29:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div>
						<div>
							<strong>fee: {parsedTransaction.fee.amount}</strong> {this.displayOperation(operationType)} <strong>{this.findAccountName(parsedTransaction.committee_member_account)}</strong>
						</div>
					</TableCell>
				);
			case 37:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div>
						<div>
							<strong>fee: {parsedTransaction.total_claimed.amount}</strong> {this.displayOperation(operationType)} <strong>{this.findAccountName(parsedTransaction.deposit_to_account)}</strong>
						</div>
					</TableCell>
				);
			case 47:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div>
						<div>
							<strong>fee: {parsedTransaction.fee.amount}</strong> {this.displayOperation(operationType)} <strong>{this.findAccountName(parsedTransaction.registrar)}</strong>
						</div>
					</TableCell>
				);
			case 62:
				return (
					<TableCell>
						<div className={`${styles['bold-text']}`}>{this.displayOperation(operationType)}</div>
						<div>ID: {parsedTransaction.id}</div>
						<strong>fee: {parsedTransaction.fee.amount}</strong> bet_place <strong>{this.findAccountName(parsedTransaction.bettor_id)}</strong>
					</TableCell>
				);
			default:
				return this.renderOther(operationType, parsedTransaction, i);
		}
	}
  
	render() {
		const {detail, key} = this.props;
		return (
			<TableRow hover={true} key={key}>
				{this.renderTransaction(detail)}
			</TableRow>
		);
	}
}

const mapStateToProps = (state) => ({
	accounts: state.accounts.accountList,
	operations: state.transactions.operations
});

export default connect(mapStateToProps)(TransactionRow);